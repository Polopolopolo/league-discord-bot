import requests

from PyQt5 import QtWidgets, uic
from PyQt5.QtWidgets import QWidget, QApplication, QLabel, QVBoxLayout
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import QUrl


class QUrlPixmap(QPixmap):
    def __init__(self, url):
        super().__init__()
        if url:
            data = requests.get(url).content
            self.loadFromData(data)


class PlayerUi(QWidget):
    def __init__(self):
        super().__init__()
        uic.loadUi("gui/player_recap.ui", self)

    @classmethod
    def build(cls, champion_pic, level, summoner_spell_pics, player_name, score, item_pics, token_pic, minions, gold):
        player_ui = PlayerUi()
        # TODO add level
        player_ui.champion_picture.setPixmap(QUrlPixmap(champion_pic))
        player_ui.summoner_spell_picture1.setPixmap(QUrlPixmap(summoner_spell_pics[0]))
        player_ui.summoner_spell_picture2.setPixmap(QUrlPixmap(summoner_spell_pics[1]))
        player_ui.summoner_name.setText(player_name)
        player_ui.score.setText(score)
        for i in range(6):
            getattr(player_ui, f"item_picture{i+1}").setPixmap(QUrlPixmap(item_pics[i]))
        player_ui.token_picture.setPixmap(QUrlPixmap(token_pic))
        player_ui.minions.setText(str(minions))
        player_ui.golds.setText(str(gold) + "k")
        return player_ui


class TeamRecapUi(QWidget):
    def __init__(self):
        super().__init__()
        uic.loadUi("gui/team_recap.ui", self)

    @classmethod
    def build(cls, color, result, gold, kills, players, bans, towers, inhibs, drakes, heralds, nashs):
        team_ui = TeamRecapUi()
        team_ui.color.setText(color)
        team_ui.result.setText(result)
        team_ui.gold.setText(str(gold) + "k")
        team_ui.kills.setText(str(kills))
        team_ui.towers.setText(str(towers))
        team_ui.inhibs.setText(str(inhibs))
        team_ui.drakes.setText(str(drakes))
        team_ui.heralds.setText(str(heralds))
        team_ui.nashors.setText(str(nashs))
        for i in range (5):
            getattr(team_ui, f"player{i+1}").setLayout(QVBoxLayout())
            getattr(team_ui, f"player{i+1}").layout().addWidget(players[i])
            # setattr(team_ui, f"player{i+1}", players[i])
            getattr(team_ui, f"ban{i+1}").setPixmap(QUrlPixmap(bans[i]))
        return team_ui


def main():
    app = QApplication([])

    import sqlite3
    from models import Team
    team = Team.from_pkey(4953187428, 100)
    team_ui = team.to_qt()
    team_ui.show()

    app.exec_()
