# LEAGUE DISCORD BOT

Please look at notes.docx for a description of the project.

## Usage

### Command Line Interface

```shell
usage: main.py [-h] {check-update,correct,store-last-game} ...

League Discord Bot CLI

positional arguments:
  {check-update,correct,store-last-game}
    check-update        Check if there are modifications to bring to the database given version
    correct             Correct the database with the new version's data
    store-last-game     Store the last game's data of a given player

optional arguments:
  -h, --help            show this help message and exit
```

#### check-update
```shell
usage: main.py check-update [-h] version

positional arguments:
  version     Version number e.g. '10.24.1'

optional arguments:
  -h, --help  show this help message and exit
```

#### correct
```shell
usage: main.py correct [-h] version

positional arguments:
  version     Version number e.g. '10.24.1'

optional arguments:
  -h, --help  show this help message and exit
```

#### store-last-game
```shell
usage: main.py store-last-game [-h] summoner_name

positional arguments:
  summoner_name

optional arguments:
  -h, --help     show this help message and exit
```


### Developers
Developers should only need the following libs:
- `utils.py`
- `api.py`
- `models.py`

#### Code examples
```python
# Return average KDA of xXDarkSasukeDu64Xx
games = api.get_games_from_summoner_name("xXDarkSasukeDu64Xx")
k, d, a = 0, 0, 0
for game in games:
    player = game["xXDarkSasukeDu64Xx"]
    k += player.kills
    d += player.deaths
    a += player.assists
k /= len(games)
d /= len(games)
a /= len(games)
```


### Work In Progress
The `./gui/` folder is supposed to provide an optional graphical interface and other graphic tools for the bot but it's not ready to use yet.
