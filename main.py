import argparse
from pprint import pprint

from api import *
from utils import *


def check_update_cb(args):
    diffs = check_diffs(args.version)

    for table, diff in diffs.items():
        print (f"== {table} ==")
        for k, v in diff.items():
            print (f"  {len(v)} {k} elements")
        print()


def correct_cb(args):
    diffs = check_diffs(args.version)
    correct_tables(diffs)


def store_last_game_cb(args):
    game_recap = get_game_info_from_summoner_name(args.summoner_name)
    if not isinstance(game_recap, dict):
        game_recap.to_database()
    else:
        print (game_recap)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="League Discord Bot CLI")
    subparsers = parser.add_subparsers()

    check_update_parser = subparsers.add_parser("check-update", help="Check if there are modifications to bring to the database given version")
    check_update_parser.add_argument("version", help="Version number e.g. '10.24.1'")
    check_update_parser.set_defaults(callback=check_update_cb)

    correct_parser = subparsers.add_parser("correct", help="Correct the database with the new version's data")
    correct_parser.add_argument("version", help="Version number e.g. '10.24.1'")
    correct_parser.set_defaults(callback=correct_cb)

    store_last_game_parser = subparsers.add_parser("store-last-game", help="Store the last game's data of a given player")
    store_last_game_parser.add_argument("summoner_name")
    store_last_game_parser.set_defaults(callback=store_last_game_cb)

    args = parser.parse_args()
    if hasattr(args, "callback"):
        args.callback(args)
    else:
        parser.print_usage()
