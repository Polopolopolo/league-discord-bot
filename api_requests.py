import requests

from models import Game


class ApiRequest:
    DVT_API_KEY = "RGAPI-930d8f52-c792-4bf9-836e-14518ae5e77a"
    URL = ""

    @classmethod
    def make_request(cls, **kwargs):
        resp = requests.get(cls.URL.format(**kwargs), headers={"X-Riot-Token": cls.DVT_API_KEY})
        return resp
    
    @classmethod
    def on_error(cls, resp):
        return {"retcode": resp.status_code, "data": resp.json()["status"]["message"]}

    @classmethod
    def on_success(cls, resp):
        raise NotImplementedError

    @classmethod
    def process_response(cls, resp):
        if resp.status_code != 200:
            return cls.on_error(resp)
        else:
            return cls.on_success(resp)


class AccountRequest(ApiRequest):
    URL = "https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/{summoner_name}"

    @classmethod
    def on_success(cls, resp):
        return {"retcode": 0, "data": resp.json()["accountId"]}


class GameListRequest(ApiRequest):
    URL = "https://euw1.api.riotgames.com/lol/match/v4/matchlists/by-account/{account_id}?endIndex=1"

    @classmethod
    def on_success(cls, resp):
        return {"retcode": 0, "data": resp.json()["matches"][0]["gameId"]}


class GameRequest(ApiRequest):
    URL = "https://euw1.api.riotgames.com/lol/match/v4/matches/{game_id}"

    @classmethod
    def on_success(cls, resp):
        return {"retcode": 0, "data": Game.from_response(resp.json())}


if __name__ == "__main__":
    # for test purpose
    resp = AccountRequest.make_request(summoner_name="Sp Holo")
    resp = AccountRequest.process_response(resp)
    print (resp)

    if resp["retcode"] == 0:
        acc_id = resp["data"]
        resp = GameListRequest.make_request(account_id=acc_id)
        resp = GameListRequest.process_response(resp)
        print (resp)
        
        if resp["retcode"] == 0:
            game_id = resp["data"]
            resp = GameRequest.make_request(game_id=game_id)
            print (GameRequest.process_response(resp)["data"])
