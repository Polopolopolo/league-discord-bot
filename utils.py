from api_requests import AccountRequest, GameListRequest, GameRequest


def get_game_info_from_summoner_name(summoner_name: str):
    """ Try to get the last game played by `summoner_name`  
    """
    resp = AccountRequest.make_request(summoner_name=summoner_name)
    p_resp = AccountRequest.process_response(resp)
    if p_resp["retcode"] != 0:
        return p_resp
    else:
        account_id = p_resp["data"]
        resp = GameListRequest.make_request(account_id=account_id)
        p_resp = GameListRequest.process_response(resp)
        if p_resp["retcode"] != 0:
            return p_resp
        else:
            game_id = p_resp["data"]
            resp = GameRequest.make_request(game_id=game_id)
            p_resp = GameRequest.process_response(resp)
            if p_resp["retcode"] != 0:
                return p_resp
            else:
                game_recap = p_resp["data"]
                return game_recap
