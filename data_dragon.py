import requests

from sql_agent import sql_agent


class DataDragonTable:
    name = ""
    url = ""
    fields = []
    dd_data = [] # data from datadragon
    db_data = [] # data from the local database

    @classmethod
    def fetch_data(cls, version):
        raise NotImplementedError

    @classmethod
    def read_db_data(cls):
        cls.db_data = sql_agent.get_all_data_of_table(cls.name)

    @classmethod
    def diff(cls):
        pkeys1 = set(x[0] for x in cls.db_data)
        pkeys2 = set(x[0] for x in cls.dd_data)

        new_pkeys = pkeys2.difference(pkeys1)
        new_elements = sorted(x for x in cls.dd_data if x[0] in new_pkeys)

        rm_pkeys = pkeys1.difference(pkeys2)
        rm_elements = sorted(x for x in cls.db_data if x[0] in rm_pkeys)

        common_pkeys = pkeys1.intersection(pkeys2)
        common_elements = set(cls.db_data).intersection(cls.dd_data)
        modified_elements_pkeys = common_pkeys.difference(x[0] for x in common_elements)
        modified_elements = sorted(x for x in cls.dd_data if x[0] in modified_elements_pkeys)

        return {"new": new_elements, "removed": rm_elements, "modified": modified_elements}

    @classmethod
    def update_table(cls, diff):
        for x in diff["modified"]:
            sql_agent.update_dd_table(cls.name, cls.fields, cls.fields[0], x)

    @classmethod
    def add_to_table(cls, diff):
        for x in diff["new"]:
            sql_agent.insert_into_dd_table(cls.name, len(cls.fields), x)

    @classmethod
    def remove_from_table(cls, diff):
        for x in diff["removed"]:
            sql_agent.remove_from_dd_table(cls.name, cls.fields[0], x[0])


class ChampiondDDTable(DataDragonTable):
    name = "Champion"
    fields = "ChampionId Name Picture".split()

    @classmethod
    def fetch_data(cls, version):
        """ Get champions data from datadragon
        """
        url = f"http://ddragon.leagueoflegends.com/cdn/{version}/data/fr_FR/champion.json"
        resp = requests.get(url)
        data = resp.json()["data"]
        cls.dd_data = [
            (
                int(v["key"]),
                v["name"],
                f"http://ddragon.leagueoflegends.com/cdn/{version}/img/champion/{k}.png"
            ) for (k, v) in data.items()
        ]


class SummonerSpellDDTable(DataDragonTable):
    name = "SummonerSpell"
    fields = "SummonerSpellId Name Picture".split()

    @classmethod
    def fetch_data(cls, version):
        """ Get summoner spells data from datadragon
        """
        url = f"http://ddragon.leagueoflegends.com/cdn/{version}/data/fr_FR/summoner.json"
        resp = requests.get(url)
        data = resp.json()["data"]
        cls.dd_data = [
            (
                int(v["key"]),
                v["name"],
                f"http://ddragon.leagueoflegends.com/cdn/{version}/img/spell/{k}.png"
            ) for (k, v) in data.items()
        ]


class ItemDDTable(DataDragonTable):
    name = "Item"
    fields = "ItemId Name Picture Description".split()

    @classmethod
    def fetch_data(cls, version):
        """ Get items data from datadragon
        """
        url = f"http://ddragon.leagueoflegends.com/cdn/{version}/data/fr_FR/item.json"
        resp = requests.get(url)
        data = resp.json()["data"]
        cls.dd_data = [
            (
                int(k),
                v["name"],
                f"http://ddragon.leagueoflegends.com/cdn/{version}/img/item/{k}.png",
                v["description"]
            ) for (k, v) in data.items()
        ]

class MapDDTable(DataDragonTable):
    name = "Map"
    fields = "MapId Name".split()

    @classmethod
    def fetch_data(cls, version):
        """ Get maps data from datadragon
        """
        url = f"http://static.developer.riotgames.com/docs/lol/maps.json"
        resp = requests.get(url)
        maps = resp.json()
        cls.dd_data = [
            (
                m["mapId"],
                m["mapName"],
            ) for m in maps
        ]

class TeamDDTable(DataDragonTable):
    name = "Team"
    fields = "TeamId Color".split()

    @classmethod
    def fetch_data(cls, version):
        """ This function does not fetch any data
        """
        cls.dd_data = [(100,"blue"), (200, "red")]


class DataDragonTableMeta:
    _TABLES = {
        "champion": ChampiondDDTable,
        "summonerspell": SummonerSpellDDTable,
        "iter": ItemDDTable,
        "map": MapDDTable,
        "team": TeamDDTable
    }

    @classmethod
    def get_table_names(cls):
        return list(cls._TABLES)

    @classmethod
    def get_table_from_name(cls, name):
        if name.lower() in cls._TABLES:
            return cls._TABLES[name]

    @classmethod
    def get_tables_from_string(cls, s):
        """ Returns a list of table classes corresponding to `s`  
        `s` is either '*' for all the tables or a string that contains table's names separated by a blank space e.g. 'champion item team'
        """
        if s == "*":
            return list(cls._TABLES.values())
        else:
            return [cls.get_table_from_name(name)for name in s.split()]
