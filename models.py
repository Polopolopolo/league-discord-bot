from datetime import datetime
import pprint
from typing import List

from sql_agent import sql_agent


class Model:
    def __repr__(self):
        return pprint.pformat(self.__dict__)


class SummonerSpell(Model):
    def __init__(self, summoner_spell_id, name, picture):
        self.summoner_spell_id = summoner_spell_id
        self.name = name
        self.picture = picture

    @classmethod
    def from_pkey(cls, summoner_spell_id):
        row = sql_agent.get_data_of_table_from_pkey("SummonerSpell", "SummonerSpellId", summoner_spell_id)
        return cls(summoner_spell_id, row["Name"], row["Picture"])


class Item(Model):
    def __init__(self, item_id, name, picture, description):
        self.item_id = item_id
        self.name = name
        self.picture = picture
        self.description = description

    @classmethod
    def from_pkey(cls, item_id):
        if not item_id:
            return cls(0, "", "", "")
        else:
            row = sql_agent.get_data_of_table_from_pkey("Item", "ItemId", item_id)
            return cls(item_id, row["Name"], row["Picture"], row["Description"])


class Champion(Model):
    def __init__(self, champion_id, name, picture):
        self.champion_id = champion_id
        self.name = name
        self.picture = picture

    @classmethod
    def from_pkey(cls, champion_id):
        if not champion_id:
            return cls("", "", "")
        else:
            row = sql_agent.get_data_of_table_from_pkey("Champion", "ChampionId", champion_id)
            return cls(champion_id, row["Name"], row["Picture"])


class Map(Model):
    def __init__(self, map_id, name):
        self.map_id = map_id
        self.name = name

    @classmethod
    def from_pkey(cls, map_id):
        row = sql_agent.get_data_of_table_from_pkey("Map", "MapId", map_id)
        return cls(map_id, row["Name"])


class Player(Model):
    def __init__(self, game_id, player_id, name, champion_id, level, summoner_spell_ids, kills, deaths, assists, item_ids,
                token_slot_id, minions_killed, gold):

        self.game_id            : int                       = game_id
        self.player_id          : int                       = player_id
        self.name               : str                       = name
        self.champion_id        : int                       = champion_id
        self.level              : int                       = level
        self.summoner_spell_ids : List[int]                 = summoner_spell_ids
        self.kills              : int                       = kills
        self.deaths             : int                       = deaths
        self.assists            : int                       = assists
        self.item_ids           : List[int]                 = item_ids
        self.token_slot_id      : int                       = token_slot_id
        self.minions_killed     : int                       = minions_killed
        self.gold               : int                       = gold

        self._champion          : Champion                  = None
        self._summoner_spells   : List[SummonerSpell]       = None
        self._items             : List[Item]                = None
        self._token_slot        : Item                      = None

    @property
    def champion(self):
        if not self._champion:
            self._champion = Champion.from_pkey(self.champion_id)
        return self._champion

    @property
    def summoner_spells(self):
        if not self._summoner_spells:
            self._summoner_spells = tuple(SummonerSpell.from_pkey(ss_id) for ss_id in self.summoner_spell_ids)
        return self._summoner_spells

    @property
    def items(self):
        if not self._items:
            self._items = tuple(Item.from_pkey(it_id) for it_id in self.item_ids)
        return self._items

    @property
    def token_slot(self):
        if not self._token_slot:
            self._token_slot = Item.from_pkey(self.token_slot_id)
        return self._token_slot

    @classmethod
    def from_response(cls, resp_json):
        players = {team["teamId"]: [] for team in resp_json["teams"]}

        for identity, player in zip(resp_json["participantIdentities"], resp_json["participants"]):

            players[player["teamId"]].append(cls(
                resp_json["gameId"],
                identity["player"]["summonerId"],
                identity["player"]["summonerName"],
                player["championId"],
                player["stats"]["champLevel"],
                (player["spell1Id"], player["spell2Id"]),
                player["stats"]["kills"],
                player["stats"]["deaths"],
                player["stats"]["assists"],
                tuple(player["stats"][f"item{i}"] for i in range(6)),
                player["stats"]["item6"],
                player["stats"]["totalMinionsKilled"],
                round(player["stats"]["goldEarned"]/1000, 2),
            ))

        return players

    @classmethod
    def from_pkey(cls, game_id, player_id):
        row = sql_agent.get_data_of_table_from_pkeys("Player", ["PlayerId", "GameId"], [player_id, game_id])
        return cls(
            game_id,
            player_id,
            row["Name"],
            row["Champion"],
            row["Level"],
            (row["SummonerSpell1"], row["SummonerSpell2"]),
            row["Kills"],
            row["Deaths"],
            row["Assists"],
            tuple(row[f"Item{i}"] for i in range(1, 7)),
            row["TokenSlot"],
            row["MinionsKilled"],
            row["Gold"]
        )

    def to_database(self):
        values = (  self.game_id, self.player_id, self.name, self.champion, self.level, *self.summoner_spell_ids,
                    self.kills, self.deaths, self.assists, *self.item_ids,*(None for _ in range(6 - len(self.item_ids))),
                    self.token_slot_id, self.minions_killed, self.gold)
        sql_agent.insert_into_table("Player", 19, values)

    def to_qt(self):
        raise NotImplementedError
        from gui.gui import PlayerUi
        champion = Champion.from_pkey(self.champion)
        summoner_spells = [SummonerSpell.from_pkey(ss) for ss in self.summoner_spell_ids]
        items = [Item.from_pkey(itm) for itm in self.item_ids]
        token = Item.from_pkey(self.token_slot_id)
        return PlayerUi.build( champion.picture, self.level, [ss.picture for ss in summoner_spells], self.name,
                                    f"{self.kills}/{self.deaths}/{self.assists}", [itm.picture for itm in items],
                                    token.picture, self.minions_killed, self.gold)


class Team(Model):
    def __init__(self, game_id, team_id, player_ids, ban_ids, towers_destroyed, inhibitors_destroyed, drakes_killed,
                heralds_killed, nashors_killed):

        self.game_id                : int                       = game_id
        self.team_id                : int                       = team_id
        self.player_ids             : List[int]                 = player_ids
        self.ban_ids                : List[int]                 = ban_ids
        self.towers_destroyed       : int                       = towers_destroyed
        self.inhibitors_destroyed   : int                       = inhibitors_destroyed
        self.drakes_killed          : int                       = drakes_killed
        self.heralds                : int                       = heralds_killed
        self.nashors_killed         : int                       = nashors_killed

        self._players               : List[Player]              = None
        self._bans                  : List[Champion]            = None

    @property
    def players(self):
        if not self._players:
            self._players = tuple(  Player.from_pkey(self.game_id, plyr_id)
                                    for plyr_id in self.player_ids)
        return self._players

    @property
    def bans(self):
        if not self._bans:
            self._bans = tuple(Champion.from_pkey(b_id) for b_id in self.ban_ids)
        return self._bans

    @classmethod
    def from_response(cls, resp_json):
        teams = []
        players = Player.from_response(resp_json)

        for team in resp_json["teams"]:
            teams.append(cls(
                resp_json["gameId"],
                team["teamId"],
                players[team["teamId"]],
                tuple(team["bans"]),
                team["towerKills"],
                team["inhibitorKills"],
                team["dragonKills"],
                team["riftHeraldKills"],
                team["baronKills"],
            ))

        return teams

    @classmethod
    def from_pkey(cls, game_id, team_id):
        row = sql_agent.get_data_of_table_from_pkeys("TeamRecap", ["TeamId", "GameId"], [team_id, game_id])
        return cls(
            game_id,
            team_id,
            tuple(row[f"Player{i}"] for i in range(1, 6)),
            tuple(row[f"Ban{i}"] for i in range(1, 6)),
            row["TowersDestroyed"],
            row["InhibitorsDestroyed"],
            row["DrakesKilled"],
            row["HeraldsKilled"],
            row["NashorsKilled"],
        )

    def to_database(self):
        values = (  self.game_id, self.team_id, *(p.player_id for p in self.player_ids),
                    *(None for _ in range(5 - len(self.player_ids))), *self.ban_ids,
                    *(None for _ in range(5 - len(self.ban_ids))), self.towers_destroyed, self.inhibitors_destroyed,
                    self.drakes_killed, self.heralds, self.nashors_killed)
        sql_agent.insert_into_table("TeamRecap", 17 , values)

        for player in self.players:
            player.to_database()

    def to_qt(self):
        raise NotImplementedError
        # from gui.gui import TeamRecapUi
        # game = Game.from_pkey(self.game_id)
        # team = Team.from_pkey(self.game_id, self.team_id)
        # players = [Player.from_pkey(self.game_id, plyr) for plyr in self.player_ids]
        # bans = [Champion.from_pkey(ban).picture for ban in self.ban_ids]
        # return TeamRecapUi.build(team.color, str(game.winner), "GOLD", "KILLS", [plyr.to_qt() for plyr in players],
        #                          bans, self.towers_destroyed, self.inhibitors_destroyed, self.drakes_killed,
        #                          self.heralds, self.nashors_killed)


class Game(Model):
    def __init__(self, game_id, game_type, game_map_id, duration, date, winner_id, team_ids):

        self.game_id    : int                   = game_id
        self.game_type  : int                   = game_type
        self.map_id     : int                   = game_map_id
        self.duration   : int                   = duration
        self.date       : str                   = date
        self.winner_id  : int                   = winner_id
        self.team_ids   : List[int]             = team_ids

        self._map       : Map                   = None
        self._winner    : Team                  = None
        self._teams     : List[Team]            = None

    @property
    def map(self):
        if not self._map:
            self._map = Map.from_pkey(self.map_id)
        return self._map

    @property
    def winner(self):
        if not self._winner:
            self._winner = [team for team in self.teams if team.team_id == self.winner_id][0]
        return self._winner

    @property
    def teams(self):
        if not self._teams:
            self._teams = tuple(Team.from_pkey(self.game_id, t_id) for t_id in self.team_ids)
        return self._teams

    def __getitem__(self, key):
        player = [p for p in self.teams[0].players() + self.teams[1].players() if p.name == key]
        if player:
            return player[0]

    @classmethod
    def from_response(cls, resp_json):
        return cls(
            resp_json["gameId"],
            resp_json["gameMode"],
            resp_json["mapId"],
            resp_json["gameDuration"],
            datetime.utcfromtimestamp(resp_json["gameCreation"] / 1000).strftime("%Y%m%d"),
            [t["teamId"] for t in resp_json["teams"] if t["win"] == "Win"][0],
            Team.from_response(resp_json))

    @classmethod
    def from_pkey(cls, game_id):
        row = sql_agent.get_data_of_table_from_pkey("GameRecap", "GameId", game_id)
        return cls(
            game_id,
            row["GameType"],
            row["Map"],
            row["Duration"],
            f"{row['DateYear']}{row['DateMonth']}{row['DateDay']}",
            row["Winner"],
            (row["Team1"], row["Team2"])
        )

    def to_database(self):
        values = (  self.game_id, self.game_type, self.map, self.duration, int(self.date[:4]), int(self.date[4:6]),
                    int(self.date[6:]), self.winner, self.team_ids)
        sql_agent.insert_into_table("GameRecap", 10 , values)

        for team in self.teams:
            team.to_database()
