from typing import List
import os
import sqlite3


class SqlAgent:
    DB_PATH = os.path.join("static", "league.db")

    def __init__(self):
        self.conn = sqlite3.connect(self.DB_PATH)
        self.conn.row_factory = sqlite3.Row

    def __del__(self):
        self.conn.close()

    def get_all_data_of_table(self, table: str):
        sql = f"SELECT * FROM {table}"
        return self.conn.execute(sql).fetchall()

    def get_data_of_table_from_pkey(self, table: str, pkey_name: str, pkey_value: int):
        sql = f"SELECT * FROM {table} WHERE {pkey_name} = ?"
        return self.conn.execute(sql, (pkey_value,)).fetchone()

    def get_data_of_table_from_pkeys(self, table: str, pkey_names: List[str], pkey_values: List[int]):
        fields_str = " AND ".join(name + " = ?" for name in pkey_names)
        sql = f"SELECT * FROM {table} WHERE {fields_str}"
        return self.conn.execute(sql, pkey_values).fetchone()

    def insert_into_table(self, table: str, nb_fields: int, values: List):
        quest_marks = "(" + ", ".join("?" * nb_fields) + ")"
        sql = f"INSERT INTO {table} VALUES {quest_marks}"
        self.conn.execute(sql, values)

    def get_games_from_summoner_names(self):
        sql = "SELECT DISTINCT GameRecap.* FROM GameRecap JOIN Player ON Player.GameId == GameRecap.GameId"
        return self.conn.execute(sql).fetchall()

    def update_dd_table(self, table: str, fields: List[str], pkey_name: str, values: List):
        fields_str = ", ".join(field + " = ?" for field in fields)
        sql = f"UPDATE {table} SET {fields_str} WHERE {pkey_name} = ?"
        values = values + (values[0],)
        self.conn.execute(sql, values)

    def insert_into_dd_table(self, table: str, nb_fields: int, values: List):
        return self.insert_into_table(table, nb_fields, values)

    def remove_from_dd_table(self, table: str, pkey_name: str, pkey_value: int):
        sql = f"DELETE FROM {table} WHERE {pkey_name} = ?"
        self.conn.execute(sql, (pkey_value,))




sql_agent = SqlAgent()
