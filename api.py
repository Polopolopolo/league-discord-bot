from typing import List

from api_requests import AccountRequest, GameListRequest, GameRequest
from data_dragon import ChampiondDDTable, SummonerSpellDDTable, ItemDDTable, MapDDTable, TeamDDTable, DataDragonTableMeta
from models import Game
from sql_agent import sql_agent


def check_diffs(version: str, tables="*") -> dict:
    """ Return a dict describing the differences between the local database and the `version` information
    """
    tables = DataDragonTableMeta.get_tables_from_string(tables)
    diffs = {}
    for table in tables:
        table.fetch_data(version)
        table.read_db_data()
        diffs[table.name] = table.diff()
    return diffs


def correct_tables(diff: dict, tables="*") -> None:
    """ Update the local database to match the `version` information  
    `version` is the argument used in `check_diffs` in order to get `diff`
    """
    tables = DataDragonTableMeta.get_tables_from_string(tables)
    for table in tables:
        table.update_table(diff[table.name])
        table.add_to_table(diff[table.name])
        table.remove_from_table(diff[table.name])


def get_games_from_summoner_name(summoner_name: str) -> List[Game]:
    """ Return the list of games (as `Game`s) that include `summoner_name`
    """
    games = sql_agent.get_games_from_summoner_names()
    return [Game.from_pkey(g["GameId"]) for g in games]
